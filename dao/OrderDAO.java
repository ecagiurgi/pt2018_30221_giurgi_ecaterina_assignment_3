package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import connection.ConnectionFactory;
import model.Order;
import model.OrderItem;

public class OrderDAO extends AbstractDAO<Order> {

	@Override
	public Order insert(Order order) {
		
		int orderId = insertOrder(order);
		
		for (OrderItem item : order.getItems()) {
			insertOrderItem(orderId, item);
		}
		
		return null;
	}

	private int insertOrder(Order order) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		String query = "INSERT INTO `Order` VALUES (null, " + order.getClient().getId() + ")";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			rs = statement.getGeneratedKeys();
            if(rs.next())
            {
                return rs.getInt(1);
            }
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, " OrderDAO:insertOrder " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return 0;
	}

	private int insertOrderItem(Integer orderId, OrderItem item) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		String query = "INSERT INTO OrderItem (orderId, productid, quantity) VALUES (" + orderId + "," + item.getProduct().getId() + "," + item.getQuantity() + ")";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			rs = statement.getGeneratedKeys();
            if(rs.next())
            {
                return rs.getInt(1);
            }
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, " OrderDAO:insertOrderItem " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return 0;
	}
	
	

}
