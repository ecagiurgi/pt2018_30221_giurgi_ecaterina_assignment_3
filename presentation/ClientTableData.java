package presentation;

import javafx.beans.property.SimpleStringProperty;

public class ClientTableData {
	
	private Integer id;
	private SimpleStringProperty name;

	public ClientTableData(Integer id, String name) {
		this.id = id;
		this.name = new SimpleStringProperty(name);
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SimpleStringProperty nameProperty() {
		return name;
	}

	
	
}
