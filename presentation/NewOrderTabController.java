package presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

public class NewOrderTabController implements Initializable {

	@FXML
	Pane productsPane;

	@FXML
	ComboBox<String> clientCbo;

	VBox vBox;

	private ClientBLL clientBLL = new ClientBLL();
	private ProductBLL productBLL = new ProductBLL();
	private OrderBLL orderBLL = new OrderBLL();

	@Override
	public void initialize(URL url, ResourceBundle resources) {
		initVBox();
		initClientCbo();
	}

	private void initClientCbo() {
		List<Client> clients = clientBLL.findAllClients();
		List<String> clientNames = clients.stream().map(client -> client.getName()).collect(Collectors.toList());
		clientCbo.setItems(FXCollections.observableArrayList(clientNames));
	}

	private void initVBox() {
		vBox = new VBox(5);
		vBox.setPadding(new Insets(5));
		ScrollPane scrollPane = new ScrollPane(vBox);
		scrollPane.setPrefHeight(200);
		scrollPane.setPrefWidth(300);
		productsPane.getChildren().add(scrollPane);
	}

	public void sendOrder() {

		Client client = findClient();
		List<OrderItem> items = createItemList();

		Order order = new Order(client, items);
		if (productBLL.verifyOrder(order)) {
			orderBLL.saveOrder(order);
			showInfo("Success!");
		} else {
			showError("Not enough quantity!");
		}

	}

	private Client findClient() {
		String clientName = clientCbo.getSelectionModel().getSelectedItem();
		return clientBLL.findClientByName(clientName);
	}

	private List<OrderItem> createItemList() {
		List<OrderItem> items = new ArrayList<>();
		for (Node node : vBox.getChildren()) {
			HBox hBox = (HBox) node;
			OrderItem item = new OrderItem();
			for (Node node2 : hBox.getChildren()) {
				if (node2 instanceof ComboBox) {
					String productName = ((ComboBox<String>) node2).getSelectionModel().getSelectedItem();
					Product product = productBLL.findProductByName(productName);
					item.setProduct(product);
				} else if (node2 instanceof TextField) {
					Integer quantity = Integer.parseInt(((TextField) node2).getText());
					item.setQuantity(quantity);
				}
			}
			;
			items.add(item);
		}
		;
		return items;
	}

	public void addProduct() {
		int nrOfProducts = vBox.getChildren().size();
		HBox hBox = new HBox(5);
		hBox.getChildren().add(createLabel(nrOfProducts));
		hBox.getChildren().add(createProductComboBox(nrOfProducts));
		hBox.getChildren().add(createQuantityTextField(nrOfProducts));
		vBox.getChildren().add(hBox);
	}

	private Label createLabel(int nrOfProducts) {
		Label label = new Label("Item: ");
		return label;
	}

	private ComboBox<String> createProductComboBox(int nrOfProducts) {
		List<Product> products = productBLL.findAllProducts();
		List<String> productNames = products.stream().map(product -> product.getName()).collect(Collectors.toList());
		ComboBox<String> productCbo = new ComboBox<>();
		productCbo.setItems(FXCollections.observableArrayList(productNames));
		return productCbo;
	}

	private TextField createQuantityTextField(int nrOfProducts) {
		TextField quantityTextfield = new TextField();
		return quantityTextfield;
	}
	
	private void showError(String text) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Exception");
		alert.setHeaderText("Exception");
		alert.setContentText(text);
		alert.show();
	}
	
	private void showInfo(String text) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success");
		alert.setHeaderText("Success");
		alert.setContentText(text);
		alert.show();
	}

}
