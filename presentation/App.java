package presentation;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application{

	public App() {
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));
		stage.setScene(new Scene(root));
	    stage.show();
	
/*		Group root = new Group();
		Scene scene = new Scene(root, 700, 400, Color.WHITE);
        stage.setTitle("Tabs Test");
        
        TabPane tabPane = new TabPane();
        
        ToggleGroup toggleGroup = new ToggleGroup();
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == null)
                    toggleGroup.selectToggle(oldValue);
                else
                    tabPane.getSelectionModel().select((Tab) newValue.getUserData());
            }
        });

        
        tabPane.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

        VBox vboxToggleOuterContainer = new VBox();
        HBox hboxToggleFirstRow = new HBox();
        HBox hboxToggleSecondRow = new HBox();

        vboxToggleOuterContainer.getChildren().addAll(hboxToggleFirstRow, hboxToggleSecondRow);

        for (int i = 0; i < 3; i++) {
            Tab tab = new Tab();
            tab.setText("Tab " + i);
            HBox hbox = new HBox();
            hbox.getChildren().add(new Label("Tab " + i));
            tab.setContent(hbox);
            tabPane.getTabs().add(tab);

            ToggleButton tb = new ToggleButton("Tab" + i);
            tb.setToggleGroup(toggleGroup);
            tb.setUserData(tab);

            if (i < 10)
                hboxToggleFirstRow.getChildren().add(tb);
            else
                hboxToggleSecondRow.getChildren().add(tb);
        }
        
        Tab clientTab = new Tab("Client Tab");
        clientTab.setContent(new Label("Clients"));
        tabPane.getTabs().add(clientTab);
        ToggleButton tb = new ToggleButton("Tab Client");
        tb.setToggleGroup(toggleGroup);
        tb.setUserData(clientTab);
        hboxToggleFirstRow.getChildren().add(tb);
        

        toggleGroup.selectToggle(toggleGroup.getToggles().get(0));

        VBox vbox = new VBox();
        vbox.getChildren().addAll(vboxToggleOuterContainer, tabPane);
        vbox.fillWidthProperty().set(true);
        root.getChildren().add(vbox);

        stage.setScene(scene);
        stage.show();*/
	}
	
	
	
	
	
	public static void main(String[] args) {
        launch(args);
    }

}
