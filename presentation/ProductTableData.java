package presentation;

import javafx.beans.property.SimpleStringProperty;

public class ProductTableData {

	private Integer id;
	private SimpleStringProperty name;
	private SimpleStringProperty quantity;
	private SimpleStringProperty price;
	
	public ProductTableData(Integer id, String name, int quantity, double price) {
		this.id = id;
		this.name = new SimpleStringProperty(name);
		this.quantity = new SimpleStringProperty(String.valueOf(quantity));
		this.price = new SimpleStringProperty(String.valueOf(price));
	}
	
	
	public ProductTableData(String name, int quantity, double price) {
		this.name = new SimpleStringProperty(name);
		this.quantity = new SimpleStringProperty(String.valueOf(quantity));
		this.price = new SimpleStringProperty(String.valueOf(price));
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name.set(name);
	}
	
	
	
	public String getName() {
		return name.get();
	}


	public int getQuantity() {
		return Integer.parseInt(quantity.get());
	}


	public double getPrice() {
		return Double.parseDouble(price.get());
	}


	public void setQuantity(String quantity) {
		this.quantity.set(quantity);
	}
	
	public void setPrice(String price) {
		this.price.set(price);
	}
	
	public SimpleStringProperty nameProperty() {
		return name;
	}
	
	public SimpleStringProperty quantityProperty() {
		return quantity;
	}
	
	public SimpleStringProperty priceProperty() {
		return price;
	}	
	
}
