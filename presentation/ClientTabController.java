package presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import bll.ClientBLL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Client;

public class ClientTabController implements Initializable {

	@FXML
    private TableView<ClientTableData> clientTable;
	
	@FXML
	private TableColumn<ClientTableData, String> nameColumn; 
	
	@FXML
	private TextField nameTF;
	
	private ObservableList<ClientTableData> data = FXCollections.observableArrayList();
	private List<Client> originalClients = new ArrayList<>();
	
	
	private ClientBLL clientBLL = new ClientBLL();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
	    clientTable.setItems(data);
        populate();	
	}
	
	@FXML
	public void addClient() {
		String name = nameTF.getText();
		data.add(new ClientTableData(null, name));
	}
	
	@FXML
	public void removeClient() {
		
		ClientTableData selectedItem = clientTable.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			data.remove(selectedItem);
		}
	}
	
	@FXML
	public void saveClients() {
		
		// Remove
		final List<Integer> clientIdsToRemove = originalClients.stream().map(Client::getId).collect(Collectors.toList());
		for (Client originalClient : originalClients) {
			for (ClientTableData clientTableData : data) {
				if (originalClient.getId() != null && clientTableData.getId() != null && originalClient.getId().equals(clientTableData.getId())) {
					clientIdsToRemove.remove(originalClient.getId());
				}
			}
		}
		clientBLL.removeClients(clientIdsToRemove);
		
		// Save
		final List<Client> newClients = new ArrayList<>();
		for (ClientTableData clientTableData : data) {
			if (clientTableData.getId() == null) {
				newClients.add(new Client(clientTableData.getName()));
			}
		}
		clientBLL.saveClients(newClients);
		
		// Update
		final List<Client> clientsToUpdate = new ArrayList();
		for (ClientTableData clientTableData : data) {
			if (clientTableData.getId() != null) {
				clientsToUpdate.add(new Client(clientTableData.getId(), clientTableData.getName()));
			}
		}
		clientBLL.updateClients(clientsToUpdate);
		
		refreshTable();
		
	}

	private void refreshTable() {
		
		originalClients.clear();
		data.clear();
		populate();
		
	}

	private void initColumns() {
		
		clientTable.setEditable(true);
		clientTable.getSelectionModel().cellSelectionEnabledProperty().set(true);
		
		nameColumn.setText("Name");
		nameColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
		nameColumn.setCellFactory(TextFieldTableCell.<ClientTableData>forTableColumn());
		nameColumn.setOnEditCommit(
		    (CellEditEvent<ClientTableData, String> t) -> {
		        ((ClientTableData) t.getTableView().getItems().get(
		            t.getTablePosition().getRow())).setName(t.getNewValue());
		});
		nameColumn.setEditable(true);
	}

	private void populate() {
		List<Client> clients = clientBLL.findAllClients();
		clients.forEach(client -> data.add(new ClientTableData(client.getId(), client.getName())));
		originalClients.addAll(clients);
	}
}
