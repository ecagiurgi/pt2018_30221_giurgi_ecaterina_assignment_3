package presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import bll.ProductBLL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Product;

public class ProductTabController implements Initializable {

	@FXML
	private TableView<ProductTableData> productTable;

	@FXML
	private TableColumn<ProductTableData, String> nameColumn;

	@FXML
	private TableColumn<ProductTableData, String> quantityColumn;

	@FXML
	private TableColumn<ProductTableData, String> priceColumn;

	@FXML
	private TextField nameTF;

	@FXML
	private TextField quantityTF;

	@FXML
	private TextField priceTF;

	private ObservableList<ProductTableData> data = FXCollections.observableArrayList();
	private List<Product> originalProducts = new ArrayList<>();

	private ProductBLL productBLL = new ProductBLL();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		productTable.setItems(data);
		initColumns();
		populate();
	}

	@FXML
	public void addProduct() {
		String name = nameTF.getText();
		int quantity = extractQuantity();
		double price = extractPrice();
		data.add(new ProductTableData(name, quantity, price));
	}

	@FXML
	public void removeProduct() {

		ProductTableData selectedItem = productTable.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			data.remove(selectedItem);
		}
	}

	@FXML
	public void saveProducts() {
		// Remove
		final List<Integer> productIdsToRemove = originalProducts.stream().map(Product::getId).collect(Collectors.toList());
		for (Product originalProduct: originalProducts) {
			for (ProductTableData productTableData : data) {
				if (originalProduct.getId() != null && productTableData.getId() != null
						&& originalProduct.getId().equals(productTableData.getId())) {
					productIdsToRemove.remove(originalProduct.getId());
				}
			}
		}
		productBLL.removeProducts(productIdsToRemove);

		// Save
		final List<Product> newProducts = new ArrayList<>();
		for (ProductTableData productTableData : data) {
			if (productTableData.getId() == null) {
				newProducts.add(new Product(productTableData.getName(), productTableData.getQuantity(), productTableData.getPrice()));
			}
		}
		productBLL.saveProducts(newProducts);

		// Update
		final List<Product> productsToUpdate = new ArrayList<>();
		for (ProductTableData productTableData : data) {
			if (productTableData .getId() != null) {
				productsToUpdate.add(new Product(productTableData.getId(), productTableData.getName(), productTableData.getQuantity(), productTableData.getPrice()));
			}
		}
		productBLL.updateProducts(productsToUpdate);

		refreshTable();
	}

	private void refreshTable() {

		originalProducts.clear();
		data.clear();

		populate();
	}

	private void initColumns() {

		productTable.setEditable(true);
		productTable.getSelectionModel().cellSelectionEnabledProperty().set(true);

		nameColumn.setText("Name");
		nameColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
		nameColumn.setCellFactory(TextFieldTableCell.<ProductTableData>forTableColumn());
		nameColumn.setOnEditCommit((CellEditEvent<ProductTableData, String> t) -> {
			((ProductTableData) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setName(t.getNewValue());
		});
		nameColumn.setEditable(true);

		quantityColumn.setText("Quantity");
		quantityColumn.setCellValueFactory(cell -> cell.getValue().quantityProperty());
		quantityColumn.setCellFactory(TextFieldTableCell.<ProductTableData>forTableColumn());
		quantityColumn.setOnEditCommit((CellEditEvent<ProductTableData, String> t) -> {
			((ProductTableData) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setQuantity(t.getNewValue());
		});
		quantityColumn.setEditable(true);

		priceColumn.setText("Price");
		priceColumn.setCellValueFactory(cell -> cell.getValue().priceProperty());
		priceColumn.setCellFactory(TextFieldTableCell.<ProductTableData>forTableColumn());
		priceColumn.setOnEditCommit((CellEditEvent<ProductTableData, String> t) -> {
			((ProductTableData) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setPrice(t.getNewValue());
		});
		priceColumn.setEditable(true);
	}

	private void populate() {
		List<Product> products = productBLL.findAllProducts();
		List<ProductTableData> productTableData = products.stream().map(product -> new ProductTableData(product.getId(), product.getName(), product.getQuantity(), product.getPrice())).collect(Collectors.toList());
		originalProducts.addAll(products);
		data.addAll(FXCollections.observableArrayList(productTableData));
	}

	private double extractPrice() {
		String price = priceTF.getText();
		try {
			return Double.parseDouble(price);
		} catch (IllegalArgumentException ex) {
			showError("Illegal number format");
		}
		return 0;
	}

	private int extractQuantity() {
		String quantity = quantityTF.getText();
		try {
			return Integer.parseInt(quantity);
		} catch (IllegalArgumentException ex) {
			showError("Illegal number format");
		}
		return 0;
	}

	private void showError(String text) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Exception");
		alert.setHeaderText("Exception");
		alert.setContentText(text);
	}

}
