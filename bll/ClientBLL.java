package bll;

import java.util.List;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	
	private ClientDAO clientDAO;
	
	public ClientBLL() {
		this.clientDAO = new ClientDAO();
	}

	public List<Client> findAllClients() {
		return clientDAO.findAll();
	}

	public void removeClients(List<Integer> clientIdsToRemove) {
		clientIdsToRemove.forEach(clientId -> clientDAO.removeById(clientId));	
	}

	public void saveClients(List<Client> newClients) {
		newClients.forEach(client -> clientDAO.insert(client));
	}
	
	public void updateClients(List<Client> clientsToUpdate) {
		clientsToUpdate.forEach(client -> clientDAO.update(client));
	}

	public Client findClientByName(String clientName) {
		List<Client> clients = this.findAllClients();
		for (Client client : clients) {
			if (client.getName().equals(clientName)) {
				return client;
			}
		}
		return null;
	}
	
}
