package bll;

import java.util.List;

import dao.ProductDAO;
import model.Order;
import model.OrderItem;
import model.Product;

public class ProductBLL {

	private ProductDAO productDAO;
	
	public ProductBLL() {
		productDAO = new ProductDAO();
	}
	
	public List<Product> findAllProducts() {
		return productDAO.findAll();
	}

	public void removeProducts(List<Integer> productIdsToRemove) {
		productIdsToRemove.forEach(productId -> productDAO.removeById(productId));	
	}

	public void saveProducts(List<Product> newProducts) {
		newProducts.forEach(product -> productDAO.insert(product));
	}
	
	public void updateProducts(List<Product> productsToUpdate) {
		productsToUpdate.forEach(product -> productDAO.update(product));
	}

	public Product findProductByName(String productName) {
		List<Product> products = productDAO.findAll();
		for (Product product : products) {
			if (product.getName().equals(productName)) {
				return product;
			}
		}
		return null;
	}

	public boolean verifyOrder(Order order) {
		
		final List<OrderItem> items = order.getItems();
		for (OrderItem item : items) {
			Product persistedProduct = productDAO.findById(item.getProduct().getId());
			if (persistedProduct.getQuantity()<item.getQuantity()) {
				return false;
			}
		}
		return true;
	}
}
