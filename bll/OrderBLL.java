package bll;

import dao.OrderDAO;
import dao.ProductDAO;
import model.Order;
import model.OrderItem;
import model.Product;

public class OrderBLL {

	private OrderDAO orderDAO;
	private ProductDAO productDAO;
	
	public OrderBLL() {
		orderDAO = new OrderDAO();
		productDAO = new ProductDAO();
	}

	public void saveOrder(Order order) {
		
		orderDAO.insert(order);
		
		for (OrderItem item: order.getItems()) {
			Product product = item.getProduct();
			product.setQuantity(product.getQuantity() - item.getQuantity());
			productDAO.update(product);
		}
	}
}
