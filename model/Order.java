package model;

import java.util.List;

public class Order {

	private Integer id;
	private Client client;
	private List<OrderItem> items;
	
	public Order(Integer id, Client client, List<OrderItem> items) {
		super();
		this.id = id;
		this.client = client;
		this.items = items;
	}

	public Order(Client client, List<OrderItem> items) {
		super();
		this.client = client;
		this.items = items;
	}

	public Order() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<OrderItem> getItems() {
		return items;
	}

	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
	
}
